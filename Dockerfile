FROM fedora:36

ENV QT_MAJOR 5
ENV QT_MINOR 15
ENV QT_PATCH 2
ENV NDK_VERSION r21e
ENV ANDROID_API 33
ENV SDK_VERSION 8512546
ENV ANDROID_BUILD_TOOLS_VERSION 31.0.0

ENV QT_CI_PACKAGES qt.qt$QT_MAJOR.${QT_MAJOR}${QT_MINOR}${QT_PATCH}.android_armv7

ENV ANDROID_NDK_ROOT /android-ndk-$NDK_VERSION
ENV ANDROID_SDK_ROOT /android-sdk-linux

RUN dnf install -q -y git wget fontconfig libX11 libX11-xcb \
        java-11-openjdk-devel unzip /usr/bin/7z make imake which \
        python3-pip perl \
    && dnf clean all -q && rm -rf /var/cache/dnf/*

ENV PATH "/$QT_MAJOR.$QT_MINOR.$QT_PATCH/android/bin:/android-sdk-linux/cmdline-tools/bin:$PATH"
ENV VERBOSE 1
RUN pip install aqtinstall==2.2.3 && aqt install-qt linux android "$QT_MAJOR.$QT_MINOR.$QT_PATCH"
RUN wget -q https://dl.google.com/android/repository/commandlinetools-linux-${SDK_VERSION}_latest.zip \
    && mkdir /android-sdk-linux && cd /android-sdk-linux \
    && unzip -q /commandlinetools-linux-${SDK_VERSION}_latest.zip && rm /commandlinetools-linux-${SDK_VERSION}_latest.zip
RUN type -p sdkmanager \
    && yes | sdkmanager --sdk_root=$ANDROID_SDK_ROOT "platform-tools" "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" "platforms;android-$ANDROID_API" | (grep -v = || true)
RUN wget -q https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip \
    && unzip -q android-ndk-${NDK_VERSION}-linux-x86_64.zip && rm android-ndk-${NDK_VERSION}-linux-x86_64.zip

ENV QT_HOME /opt/Qt/$QT_MAJOR.$QT_MINOR.$QT_PATCH

